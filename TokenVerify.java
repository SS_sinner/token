package com.joe.interceptor;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
/*
    eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBbGxvdyI6WyJyZWFkIiwid3JpdGUiLCJzaWduIiwiYWRtaW4iXX0.b8Sy44sBRY7nExiypI2E2q_StnKv_-OxGTrINio75AI
    eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBbGxvdyI6WyJyZWFkIiwid3JpdGUiLCJzaWduIiwiYWRtaW4iXX0.b8Sy44sBRY7nExiypI2E2q_StnKv_-OxGTrINio75AI
    eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBbGxvdyI6WyJyZWFkIiwid3JpdGUiLCJzaWduIiwiYWRtaW4iXX0.b8Sy44sBRY7nExiypI2E2q_StnKv_-OxGTrINio75AI
    eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBbGxvdyI6WyJyZWFkIiwid3JpdGUiLCJzaWduIiwiYWRtaW4iXX0.b8Sy44sBRY7nExiypI2E2q_StnKv_-OxGTrINio75AI
*/

import java.io.UnsupportedEncodingException;
import java.util.Base64;

import static java.security.KeyRep.Type.SECRET;
/*
    使用过程：0.修改Algorithm.HMAC256("!Q@W#E$R")中的!Q@W#E$R部分，转化为自己要验证的密钥，在 create_token()和verify()中都进行修改
            1.执行一次这个文件，create_token()会生成一串token
            2.再将生成的token复制到verify()里的token内
            3.再执行一次这个文件，如果验证不成功则会报错。
*/


public class TokenVerify {
    public static void main(String[] args) {
        try {
            create_token();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        verify();
    }
//    产生token
    public static void create_token() throws UnsupportedEncodingException {
        String s = "[";
        StringBuilder sb = new StringBuilder(s);
        sb.append("read").append(',').append("write").append(',').append("sign").append(',').append("admin").append("]");
        String value = sb.toString();
        System.out.println(value);
//        第一部分是用base64隐藏你的HMAC256方法（他只是方法的编码）。eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9
//        第二部分是value，是你想要隐藏的值。他在第二部分eyJBbGxvdyI6InJlYWR3cml0ZXNpZ25hZG1pbiJ9
//        第三部分是验证部分。包含的内容是你用HMAC256对第一第二部分加密后的base64编码
        String token = JWT.create().withClaim("Allow",value).sign(Algorithm.HMAC256("MTgzLDExMSwyNDQsOCwxNDYsMzUsMjAzLDEwMiwzMiwyNTUsMTgxLDE3LDIzNywxNzUsMTQ2LDE3NywxNTgsMTE5LDE3LDEwMSwxNjgsMTM0LDIzMSwxODAsOTEsMTkzLDE1MiwyMjIsMTE1LDU3LDE3OCwyMjU="));
        System.out.println("Token:");
        System.out.println(token);
    }


//    验证token
    public static void verify(){
        String token0 = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBbGxvdyI6WyJyZWFkIiwid3JpdGUiLCJzaWduIiwiYWRtaW4iXX0.teUgpn-0FG7TbydAMPev2bZCCv27R7gByw670hy-yTM";
        String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBbGxvdyI6WyJyZWFkIiwid3JpdGUiLCJzaWduIiwiYWRtaW4iXX0.b8Sy44sBRY7nExiypI2E2q_StnKv_-OxGTrINio75AI";
        JWTVerifier jwt = null;
//        183 111 244 8 146 35 203 102 32 255 181 17 237 175 146 177 158 119 17 101 168 134 231 180 91 193 152 222 115 57 178 225
        String ba64 =   "MTgzIDExMSAyNDQgOCAxNDYgMzUgMjAzIDEwMiAzMiAyNTUgMTgxIDE3IDIzNyAxNzUgMTQ2IDE3NyAxNTggMTE5IDE3IDEwMSAxNjggMTM0IDIzMSAxODAgOTEgMTkzIDE1MiAyMjIgMTE1IDU3IDE3OCAyMjU=";
//        String ba64 = "MTgzLDExMSwyNDQsOCwxNDYsMzUsMjAzLDEwMiwzMiwyNTUsMTgxLDE3LDIzNywxNzUsMTQ2LDE3NywxNTgsMTE5LDE3LDEwMSwxNjgsMTM0LDIzMSwxODAsOTEsMTkzLDE1MiwyMjIsMTE1LDU3LDE3OCwyMjU=";
        byte[] secret = {(byte)183,(byte)111,(byte)244,(byte)8,(byte)146,(byte)35,(byte)203,(byte)102,(byte)32,(byte)255,(byte)181,(byte)17,(byte)237,(byte)175,(byte)146,(byte)177,(byte)158,(byte)119,(byte)17,(byte)101,(byte)168,(byte)134,(byte)231,(byte)180,(byte)91,(byte)193,(byte)152,(byte)222,(byte)115,(byte)57,(byte)178,(byte)225};
        byte[] b= Base64.getDecoder().decode(ba64);
        jwt = JWT.require(Algorithm.HMAC256(b)).build();
        DecodedJWT verify = jwt.verify(token);
        System.out.println("-----------------验证token------------------");
        System.out.println(verify.getClaim("Allow").asString());
        System.out.println(verify.getHeaderClaim("alg").asString());
        System.out.println(verify.getHeaderClaim("typ").asString());

    }

}

